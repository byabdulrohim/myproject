require 'test_helper'

class HomesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get homes_index_url
    assert_response :success
  end

  test "should get profile" do
    get homes_profile_url
    assert_response :success
  end

  test "should get customer" do
    get homes_customer_url
    assert_response :success
  end

  test "should get product" do
    get homes_product_url
    assert_response :success
  end

  test "should get gallery" do
    get homes_gallery_url
    assert_response :success
  end

  test "should get contact" do
    get homes_contact_url
    assert_response :success
  end

end
