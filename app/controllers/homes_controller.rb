class HomesController < ApplicationController
  def index
    @info = "Index"
    @active_home ="active"
    @title ="Home - Tri Saudara Sentosa Industri"
  end

  def company
    @active_company ="active"
    @info = "Company"
    @title= "Company - Tri Saudara Sentosa Industri"
  end

  def facilities
    @active_facilities ="active"    
    @info = "Facilities"
    @title ="Facilities - Tri Saudara Sentosa Industri"
  end

  def contact
    @info = "Contact Us"
    @active_contact ="active"
    @title ="Contact - Tri Saudara Sentosa Industri"    
  end

  def career
    @info = "Career"
    @active_career = "active"
    @title = "Career - Tri Saudara Sentosa Industri"
  end
  
end
