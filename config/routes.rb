Rails.application.routes.draw do
  get '/', to: 'homes#index'
  get '/company', to: 'homes#company'
  get '/facilities', to: 'homes#facilities'
  get '/contact-us', to: 'homes#contact'
  get '/career', to: 'homes#career'

  root to: 'homes#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
